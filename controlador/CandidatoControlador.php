<?php
require __DIR__.'/../modelo/Modelo.php';
require __DIR__.'/../modelo/ModeloCandidato.php';
class Controller {
 public $model;

    public function __construct() {
        $this->model = new ModeloCandidato();
    }

    public function invoke() {
       $ids= $_POST['id'];

       $infoCandidato = $this->model->getCandidato($ids);
       $arr = json_decode($infoCandidato);
       $nombre=  $arr[0][4];
       $apellidos = $arr[0][2]." ".$arr[0][3];
       $partido= $arr[0][10];
       $DNI = $arr[0][1];
       $fechaNacimiento = $arr[0][8];
       $nacimiento = $arr[0][6]. " ".$arr[0][5];
       
       
       $estudios =$arr[0][23]." en ".$arr[0][25];
      
       if($arr[0][24]!="Si"){
           $estudios = $estudios." no completos";
       }
      $trabajo = $arr[0][21]." ".$arr[0][22];
      $antecedentes = [];
      $antecedentes[0] = $arr[0][28]." ".$arr[0][29]. " ".$arr[0][30];      
      $antecedentes[1] = $arr[0][32]." ".$arr[0][33]. " ".$arr[0][34];      
      $antecedentes[2] = $arr[0][35]." ".$arr[0][36]. " ".$arr[0][37];
      $inmuebles  =$arr[0][16];
      $autos= $arr[0][17];
      $bienes = $arr[0][18];
    $ingresos = $arr[0][19];
    $delitos = $arr[0][46]." ".$arr[0][47];
      
      include './vista/CandidatoVista.php';
     
            }
    }

$controller = new Controller();
$controller->invoke();
  ?>


