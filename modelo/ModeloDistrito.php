<?php

class ModeloDistrito extends Modelo  {

    public function __construct() {
        parent::__construct();
    }

    public function getLista() {

        $sql = "SELECT * FROM departamento where `departamentoEstado`=1";
        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($outp);
        return json_encode($outp);
    }
    
    public function getDistrito($id) {

        $sql = "SELECT distrito.distritonombre as nom FROM distrito where `IdDistrito` = $id";
           $acentos = parent::getConn()->query("SET NAMES 'utf8'");

        $result = parent::getConn()->query($sql);
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
      //  echo json_encode($outp);
        return json_encode($outp);
    }
public function  getListaPostulantes($id){
       $sql = "SELECT candidato.`idcandidato` as 'id',distrito.DistritoNombre as distrito , CONCAT(candidato.`candidatonombre`, ' ' ,candidato.`candidatoapellido` ) AS 'nombre',partidopolitico.`partidopoliticonombre` as 'partidonombre' , partidopolitico.`idpartidopolitico` as 'idpartido'FROM `candidato` INNER JOIN partidopolitico on candidato.`idagrupacion` =partidopolitico.`idpartidoPolitico`  INNER JOIN distrito on candidato.IdDistritoPostula=distrito.IdDistrito WHERE candidato.`IdDistritoPostula` =$id";
    //   $sql = "SELECT * FROM `departamento`";
       $acentos = parent::getConn()->query("SET NAMES 'utf8'");

       $result = parent::getConn()->query($sql);
        
          $outp = array();
        $i=0 ;
        while ($fila = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
           $codigo = $fila ["id"];
        
            $Nombre = $fila ["nombre"];
            $partido = $fila ["partidonombre"];
            $distrito = $fila ["distrito"];
    
            $partidoId =$fila ["idpartido"];
             $salida = array('id'=>$codigo,'nombre'=>$Nombre,'distrito'=>$distrito,'partidonombre'=>$partido,'idpartido'=>$partidoId  );
             $outp[$Nombre]=$salida;
              
               }
        return json_encode($outp, JSON_UNESCAPED_UNICODE );
    
    
}

public function  getListaPostulantesTodos(){
    $sql = "SELECT candidato.`idcandidato` as 'id',distrito.DistritoNombre as distrito , CONCAT(candidato.`candidatonombre`, ' ' ,candidato.`candidatoapellido` ) AS 'nombre',partidopolitico.`partidopoliticonombre` as 'partidonombre' , partidopolitico.`idpartidopolitico` as 'idpartido'FROM `candidato` INNER JOIN partidopolitico on candidato.`idagrupacion` =partidopolitico.`idpartidoPolitico`  INNER JOIN distrito on candidato.IdDistritoPostula=distrito.IdDistrito WHERE candidato.`IdDistritoPostula` IS NOT NULL";
    //   $sql = "SELECT * FROM `departamento`";
       $acentos = parent::getConn()->query("SET NAMES 'utf8'");

       $result = parent::getConn()->query($sql);
        
          $outp = array();
        $i=0 ;
        while ($fila = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
           $codigo = $fila ["id"];
        
            $Nombre = $fila ["nombre"];
           // echo $Nombre;
            $partido = $fila ["partidonombre"];
            $distrito = $fila ["distrito"];
    
            $partidoId =$fila ["idpartido"];
             $salida = array('id'=>$codigo,'nombre'=>$Nombre,'distrito'=>$distrito,'partidonombre'=>$partido,'idpartido'=>$partidoId  );
             $outp[$Nombre]=$salida;
              
               }
        return json_encode($outp, JSON_UNESCAPED_UNICODE );
     
    
}

}
