 <?php $nombre=  $arr[0][4];
       $apellidos = $arr[0][2]." ".$arr[0][3];
       $partido= $arr[0][10];
       $DNI = $arr[0][1];
       $fechaNacimiento = $arr[0][8];
       
       $nacimiento = $arr[0][6]. " ".$arr[0][5];
       $estudios ="-".$arr[0][23];
       if(strlen($arr[0][25])>2){
       $estudios ="-".$arr[0][23]." - ".$arr[0][25];
       }
       
       
      $trabajo = "";
      if (strlen($arr[0][21])>0){
      $trabajo = "-".$arr[0][21]." ".$arr[0][22];
          
      }
      $antecedentes = [];
      $antecedentes[] = "-Numero de veces que postula ".$arr[0][13]; 
      if ($arr[0][27] == "Si"){
      $antecedentes[] = "-".$arr[0][30]." en ".$arr[0][28]." ".$arr[0][29];      
      }
      if ($arr[0][31] == "Si"){
      
      $antecedentes[] = "-".$arr[0][34]." en ".$arr[0][32]. " ".$arr[0][33];      
      }
      if (strlen($arr[0][35])>2){
      $antecedentes[] = "-Militante en ".$arr[0][35]." ".$arr[0][36]." ".$arr[0][37];      
          
      }
      if (strlen($arr[0][38])>2){
      $antecedentes[] = "-Militante en ".$arr[0][39]." ".$arr[0][40]." ".$arr[0][41];      
          
      }
      if (strlen($arr[0][42])>2){
      $antecedentes[] = "-Militante en ".$arr[0][43]." ".$arr[0][44]." ".$arr[0][45  ];      
          
      }
      $antecedentes[]= $arr[0][58];
      $bienes = "";
      $inmuebles  =$arr[0][16];
      $autos= $arr[0][17];
      if ($inmuebles != "No declara"){
          $bienes= $inmuebles;
      }
      if (strlen($bienes)>2 && strlen($autos)>0 && $autos!="No declara"){
          $bienes ="-".$bienes ." , ".$autos;
      }
      $valorBienes = $arr[0][18];
    $ingresos = $arr[0][19];
       $empresas  = [];
       if (strlen($arr[0][52])>2){
           $empresas[]= "-".$arr[0][53]." ".$arr[0][52]." ".$arr[0][54];
           
       }
       
       if (strlen($arr[0][55])>2){
           $empresas[]= "-".$arr[0][56]." ".$arr[0][55]." ".$arr[0][57];
           
       }
       $procesos ="";
       if (strlen($arr[0][46])>2){
       $procesos = "-".$arr[0][46]." ".$arr[0][45]." ".$arr[0][47];
       }
       $delitos="";
       if(strlen($arr[0][50])>2){
       $delitos ="-".$arr[0][50]." ".$arr[0][49];
           
       }
       $postulaA= "Provincia ";
      
        if ($arr[0][12]==" Alcalde Distrital"){
           
       $postulaA= "Distrito ";
       }
       
       $postulaA= $postulaA ." ".$arr[0][15];
      
        if ($arr[0][12]=="Gobernador Regional"){
           
       $postulaA= "Región Arequipa ";
       }
       
       $procesoComentario= $arr[0][51];
       ?>
  <style type="text/css">
    .container{
      font-family: helvetica;
    }
    .box-candidato{
      border-left: 3px solid #a62b30;
    }
    .img-candidato img{
      border-radius: 30px;
      width: 250px;
      box-shadow: 0px 30px 100px #999999;
    }
    .box-candidato h1{
      color: #a62b30;

      }
    .color-red{
      color: #a62b30;
    }
    .descripcion img{
      width: 50px;
      height:50px;
    }
   
    .text-bold{
      font-weight: bold;
    }
    .text-titulo{
      font-size: 36px;
    }
    .text-subtitulo{
      font-size: 26px;
    }
    .text-contenido{
      font-size: 18px;

    }    
  </style>


 <div class="container">
        <div class="row">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-10">
            <div class="row mb-5">
              <div class="col-lg-4    img-candidato">
                <?php 
                    echo "<img class=\"  img-fluid    \" src=\"imagenes/fotoscandidatos/$ids.jpg\" alt=\"Chania\">";

                ?>
              </div>
              <div class="col-lg-8 px-5 py-4 box-candidato">
                <h1 class=" display-4"><strong><?php echo $apellidos; ?></strong> </h1>
                <h1 class=" display-4"><strong><?php echo $nombre; ?></strong> </h1>
                <h1 class=" text-muted mb-5"><strong><?php echo $partido;?> </strong></h1>
                <h3 class="text-muted"><strong>DNI:</strong> <?php echo " ".$DNI;?></h3>
                <h3 class="text-muted"><strong>Edad:</strong><?php echo " ".$fechaNacimiento;?> </h3>
                              <h3 class="text-muted"><strong>Postula a </strong><?php echo " ".$postulaA;?> </h3>

              </div>
            </div>
            <div class="my-5 pt-5 descripcion">

              <div class="mr-4 my-5 row">
                <img class="  mx-4" src="./imagenes/todoslosiconosylogos/trabajo.jpg"> <h1 class="color-red my-3"><strong>Formacion profesional</strong></h1>
              </div >
              <h2 class="mb-5 text-contenido"><?php echo  $estudios?></h2>
              
            
      
              <div class="mr-4 my-5 row">
                 <img class=" mx-4" src="./imagenes/todoslosiconosylogos/trabajo_experiencia.jpg"> <h1 class="color-red my-3"><strong>Experiencia laboral</strong></h1>
              </div>
              <h2 class="mb-5 text-contenido"><?php echo  $trabajo ?></h2>
              
              
              
      
              <div class="mr-4 my-5 row">
                <img class=" mx-4" src="./imagenes/todoslosiconosylogos/antecedente_politico.jpg"> <h1 class="color-red my-3"><strong>Antecedentes Políticos</strong></h1>
              </div>
              <?php
                              foreach ( $antecedentes as $an){
               echo "<h2 class='text-contenido'>$an</h2>";
                                  
                              }
                
              ?>
              
              
              <div class="mr-4 my-5 row" >
                 <img class=" mx-4" src="./imagenes/todoslosiconosylogos/experiencia_economica.jpg"> <h1 class="color-red my-3"><strong>Información económica</strong></h1>
              </div>
              <h2 class="text-contenido"><strong>Bienes :</strong></h2>
             
              <p class="text-contenido"><?php echo  $bienes ?></p>
              
              
              <h2 class="text-contenido"><strong>Valor de Bienes :</strong></h2>
              
              <p class="text-contenido"><?php echo  $valorBienes?></p>
              
              <h2 class="text-contenido"><strong>Ingresos :</strong></h2>
              
             
              <p class="mb-5 text-contenido"><?php echo  $ingresos?></p>
              
              
              <h2 class="text-contenido"><strong>Empresas :</strong></h2>
              
              <?php
                              foreach ( $empresas as $an){
               echo "<h2 class='text-contenido'>$an</h2>";
                                  
                              }
                
             ?>
              <div class="mr-4 my-5 row">
                <img class=" mx-4" src="./imagenes/todoslosiconosylogos/judicial.jpg"> <h1 class="color-red my-3"><strong>Antecedentes Jurídicos</strong></h1>
              </div>
              <h2 class="text-contenido"><strong>Procesos :</strong></h2>
              
              <?php
               echo "<h2 class='text-contenido'>$procesos</h2>";
             ?>              

 
              <h2 class="text-contenido"><strong>Denuncias en Fiscalia :</strong></h2>
              
              <?php
               echo "<h2 class='text-contenido'>$delitos</h2>";
             ?>
                <?php
               echo "<h2 class='text-contenido'>$procesoComentario</h2>";
             ?>  
            </div>
          </div>
          <div class="col-lg-1">
          </div>
            
        </div>
        
        
    </div>
   