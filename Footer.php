
    <!-- Footer -->
    <footer class="py-4 bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <p class="text-white text-footer ">Copyright &copy; PolitiQuien 2018 </p>
          </div>
          <div class="col-lg-6">
            <p class="text-footer text-white text-bold">Auspiciadores:</p>
              <div class="row">
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_unsa.jpg" alt="Chania"></a>
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_idea.jpg" alt="Chania"></a>
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_rpp.jpg" alt="Chania"></a>
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_republica.jpg" alt="Chania"></a>
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_yaravi.jpg" alt="Chania"></a>
                <a href="" class="m-2"><img class="img-responsive rounded " src="imagenes/todoslosiconosylogos/logo_elbuho.jpg" alt="Chania"></a>
              </div>
            
          </div>
          
        </div>
      </div>
      <!-- /.container -->
    </footer>