
<!-- Navigation -->
<script>

    $(document).on('click', '.reCan', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Partido.php', {'id': targetId});

        console.log(targetId);
    });
    $(document).on('click', '.irProvincia2', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Provincia.php', {'idProvincia': targetId,'enfocar':1});

        console.log(targetId);
    });
    $(document).on('click', '.irDistrito2', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Distrito.php', {'idDistrito': targetId,'enfocar':1});

        console.log(targetId);
    });


</script>
<style type="text/css">
    body{
        font-family: Myriad pro;
    }
    .text-logo{
        font-size: 24px;
    }
    .text-menu{
        font-size: 20px;
    }

    .text-bold{
      font-weight: bold;
    }
    .text-tabla{
        font-size: 18px;
    }
    .text-titulo{
      font-size: 40px;
    }
    .text-subtitulo{
      font-size: 26px;
    }
    .text-contenido{
      font-size: 18px;

    }    
    .text-ai{
        font-size: 36px;
        text-shadow: 0px 0px 10px black;
    }
    .color-rojo{
        color:#a62b30;

    }
    .color-gris{
        color: #464646;
    }



</style>
<div class="container">
    <div class="row h-100">
        <div class ="col-lg-2 col-sm-2 my-auto pb-2 ">
            <a href="./index.php">
            <img class="img-responsive rounded"  src="imagenes/fwlogopolitiquien/mainLogo.jpeg" alt="Chania">
            </a>
        </div>

        <div class="col-lg-8 h-100">
            
            <div class="row  h-50">
                <div class="text-center col-md-12 col-sm-12 ">
                    <p class=" text-subtitulo-io text-bold">Elecciones Regionales y Municipales 2018 Arequipa</p>
                    <nav class="navbar navbar-expand-lg navbar-expand-sm navbar-light px-auto ">

                        <div class="collapse navbar-collapse mx-5" id="navbarNavDropdown">

                            <ul class="nav navbar-nav navbar-expand-sm ">
                                <div class="col-lg-4 col-sm-4">
                                    <li class="nav-item dropdown misBordes  ">
                                        <a class="nav-link dropdown-toggle  text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Candidatos
                                        </a>
                                        <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                                            <a  id=0 class = " dropdown-item irDistrito2"> Distritos</a>
                                            <a id=0 class="dropdown-item irProvincia2" >Provincias</a>
                                            <a class="dropdown-item" href="./">Región</a>
                                        </div>
                                    </li>

                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <li class="nav-item dropdown  misBordes ">
                                        <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Agrupación Política
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <?php
                                            require __DIR__ . '/modelo/Modelo.php';

                                            require __DIR__ . '/modelo/ModeloPartido.php';
                                            $model = ModeloPartido::getInstance();
                                            $we = $model->getLista();
                                            $array = json_decode($we, true);
                                            foreach ($array as $valor) {
                                                echo "<a class=\"dropdown-item  reCan\" id=\"$valor[IdPartidoPolitico]\" href=\"#\">$valor[PartidoPoliticoNombre]</a>";
                                            }
                                            ?>
                                        </div>
                                    </li>
                                </div>

                                <div class="col-lg-4 col-sm-4">
                                    <li class="nav-item dropdown misBordes  " >
                                        <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Provincia
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <a id="1" class="dropdown-item irProvincia2" >Arequipa</a>
                                            <a id="8" class="dropdown-item irProvincia2" >Islay</a>
                                           </div>
                                    </li>


                                </div>
                            </ul>
                        </div>
                    </nav>
                </div>


            </div>

        </div>
        <div class ="col-lg-2 col-sm-2 my-auto pb-2 ">
            <img class="img-responsive rounded" src="imagenes/fwlogopolitiquien/hack.PNG" alt="Chania">

        </div>
    </div>

</div>
<hr class="style13">
